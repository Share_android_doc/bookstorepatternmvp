package com.example.kimsoerhrd.bookstoredemopattern.base;

public interface SubMvpView {
    void onCreate();

    void onStart();

    void onResume();

    void onPause();

    void onStop();

    void onDestroy();

    void attachParentMvpView(MvpView mvpView);
}
