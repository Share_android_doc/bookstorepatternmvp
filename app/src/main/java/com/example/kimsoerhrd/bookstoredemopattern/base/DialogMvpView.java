package com.example.kimsoerhrd.bookstoredemopattern.base;

public interface DialogMvpView {
    void dismissDialog(String tag);
}
